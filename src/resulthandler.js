import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Control from 'ol/control/Control';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import {Icon, Style, Stroke} from 'ol/style';
import WKT from 'ol/format/WKT';

import escape from 'escape-html';

import {htmlToElement} from './util';
import {addClass, removeClass, replaceClass} from './util';

import {parse as parseWkt} from 'wkt';


// Current value is always the next available id.
let layerIdCounter = 0;

export class SimpleGeocoderResultHandler {
  /*
   * @param opts The options, a object as follows:
   *  opts.fitOpts: Object, the raw options to pass to Openlayers View#fit().
   *    With this you can change the animation that is shown when going to a place by Self#go().
   */
  constructor(view, opts) {
    const layerId = layerIdCounter++;
    this._layerName = `ol-flexible-geocoder-layer-${layerId}`; // TODO append random number.
    this.view = view;
    this._state = SimpleGeocoderResultHandler.State.CLEAR;
    this._opts = (opts != null) ? opts : {};

    // default options for the View#fit() Openlayers function.
    // See also https://openlayers.org/en/latest/apidoc/module-ol_View-View.html#fit
    if (this._opts.fitOpts == null) {
      this._opts.fitOpts = {
        duration: 500, // 500ms: The duration of the animation in milliseconds.
      };
    }

    this._iconStyle = new Style({
      image: new Icon({
        anchor: [0.5, 45],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: 'marker-red.png'
      })
    });

    this._layerStyle = new Style({
      stroke: new Stroke({
        //color: '#ff6200',
        color: 'rgba(0,60,136,0.5)',
        width: 4
      }),
    });


    this._source = new VectorSource();
    this._layer = new VectorLayer({
      name: this._layerName,
      source: this._source,
      style: this._layerStyle,
    });
  }

  get layer() {
    return this._layer;
  }

  get state() {
    return this._state;
  }

  set state(state) {
    this.clear();
    this._state = state;
  }

  _render(geom) {
    const feature = new Feature(geom);
    if(geom instanceof Point) {
      feature.setStyle(this._iconStyle);
    }
    this._source.addFeatures([feature]);
  }

  /*
   * @param where openlayers Coordinate type
   */
  preview(geom, doGoto = false) {
    if(!doGoto) {
      this.state = SimpleGeocoderResultHandler.State.PREVIEWING;
      this._render(geom);
    } else {
      throw Error("unimplemented!");
    }
  }

  /*
   * @param where Openlayers Geometry
   */
  go(geom) {
    this.state = SimpleGeocoderResultHandler.State.VIEWING;
    this._render(geom);
    this.view.fit(geom, this._opts.fitOpts);
  }

  clear() {
    this._source.clear(true);
  }
}
SimpleGeocoderResultHandler.State = Object.freeze({CLEAR:0,VIEWING:1,PREVIEWING:2});
