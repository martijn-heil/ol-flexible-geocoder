import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Control from 'ol/control/Control';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import {Icon, Style, Stroke} from 'ol/style';
import WKT from 'ol/format/WKT';

import escape from 'escape-html';

import {htmlToElement} from './util';
import {addClass, removeClass, replaceClass} from './util';

import {parse as parseWkt} from 'wkt';

import {SimpleGeocoderResultHandler} from './resulthandler';
import PDOKLocatieServerProvider from './pdokprovider';



class ResultList extends HTMLElement {
  get results() {
    return this._results;
  }

  render(results) {
    const ol = document.createElement("ol");
    ol.className = "ofg-sgc-search-results";

    this._results = results.map(it => {
      const li = document.createElement("li");
      const a = document.createElement("a");
      a.setAttribute("href", "#");
      a.innerHTML = it.html;
      li.append(a);

      li.addEventListener("click", e => {
        e.stopPropagation();
        this.go();
      });

      // This is apparently needed to prevent Openlayers from generating a false "singleclick" event on the map,
      // when the control is above the map surface.
      li.addEventListener("pointerdown", e => {
        e.stopPropagation();
      });

    li.addEventListener("mouseover", e => {
      this._select(li);
    });

      return {data: it, elem: li};
    });
    this._results.forEach(it => ol.append(it.elem));
    this.append(ol);
  }

  _deselect() {
    if(this._selected != null) {
      this._selected.elem.classList.remove("ofg-sgc-selected-search-result");
      this._selected = null;
    }
  }

  _select(what) {
    let result = null;
    if(what instanceof Element) {
      result = this._results.find(it => it.elem === what);
    } else if(what.data != null && what.elem != null) {
      result = what;

    } else if (Number.isInteger(what)) {
      if (what < 0) { throw Error(`Invalid array index. ${what}`); }
      result = this._results[what];
    } else {
      result = this._results.find(it => it.data === what);
      if(result == null) { throw Error(); }
    }
    if (result === this._selected) { return; }
    this._deselect();

    result.elem.classList.add("ofg-sgc-selected-search-result");
    this._selected = result;

    const event = new Event("selectResult");
    event.data = result.data;
    this.dispatchEvent(event);
  }

  selectUp() {
    const currentIndex = this._results.findIndex(it => it === this._selected);
    if (currentIndex == -1) { return; }
    const newIndex = currentIndex + -1;
    if (newIndex <= -1) {
      this._selected = null;
    } else {
      this._select(newIndex);
    }
  }

  selectDown() {
    const currentIndex = this._results.findIndex(it => it === this._selected);
    const newIndex = currentIndex + 1;
    if (newIndex >= this._results.length) { return; }
    this._select(newIndex);
  }

  go() {
    if (this._selected == null) { return; }
    const event = new Event("goResult");
    event.data = this._selected.data;
    this.dispatchEvent(event);
  }

  get selected() {
    return this._selected;
  }
}
customElements.define('result-list', ResultList);

// TODO list:
// - implement a destroy function which unregisters the event listeners.
export class SimpleGeocoderControl extends Control {
  constructor(provider, opts) {
    const placeholder = (opts.placeholder != null) ? escape(opts.placeholder) : "Search...";
    const html = `` +
    `<div class="ofg ofg-sgc-container ol-unselectable ol-control ofg-sgc-collapsed">` +
      `<button class="ofg-sgc-search-btn ofg-sgc-glass" title="${placeholder}"></button>` +
      `<form class="ofg-sgc-search-form">` +
        `<input type="search" placeholder="${placeholder}" class="ofg-sgc-search-query-field ofg-sgc-hidden">` +
        `<input type="submit" hidden>` +
      `</form>` +
    `</div>`;
    const container = htmlToElement(html);
    super({element: container});
    this._resultHandlerIsInitialized = false;
    this._resultHandler = new SimpleGeocoderResultHandler();

    container.querySelector(".ofg-sgc-search-btn").addEventListener("click", e => this.toggleState());
    container.querySelector(".ofg-sgc-search-form").addEventListener("submit", e => this._handleSubmit(e));

    // Prevent out-of-order and out-of-date futures causing unwanted side-effects.
    // This isn't a perfect solution, but it does work.
    // pseq is a sequence number that prevents race conditions where out-of-date or out-of-order promises run and cause unwanted out-of-date side effects.
    //
    // 1. event gets handled
    // 2. first available sequence number (value of seqnum1) is stored in local scope.
    // 3. seqnum1 is incremented so it is a new sequence number for the next handler to take.
    // 4. results are received.
    // 5. if the captured sequence number (my sequence number) is greater or equal to the last sequence number that was completed (seqnum2),
    //      then it's in good order and execution shall proceed.
    // 6. seqnum2 is set to the captured sequence number + 1 for the next promise to check itself against.
    //
    // From this follows that you can assign seqnum2 the value of seqnum1 at any point in execution,
    // (outside of this event handler) to invalidate all currently open promises using this sequence number.
    let seqnum1 = 0;
    let seqnum2 = 0;
    container.querySelector(".ofg-sgc-search-query-field").addEventListener("input", e => {
      const mySeq = seqnum1++;
      (async () => {
        const a = await provider.suggest(e.target.value);
        if (mySeq >= seqnum2) { seqnum2 = mySeq + 1; this.results = a; } else { console.log("out of order!"); }
      })();
    });
    this._invalidatePendingSuggestionRequests = () => {
      seqnum2 = seqnum1;
    }

    // Close when user clicks somewhere else in the document.
    document.addEventListener("click", e => {
      if (!container.contains(e.target) && this.state == SimpleGeocoderControl.State.EXPANDED) {
        this.state = SimpleGeocoderControl.State.COLLAPSED;
      }
    });

    // Clear the preview features when user hovers outside of result list
    document.addEventListener("mouseover", e => {
      if(this.state == SimpleGeocoderControl.State.COLLAPSED) { return; }
      const resultList = container.querySelector(".ofg-sgc-search-results");
      if(resultList != null && !resultList.contains(e.target)) {
        if(this._resultHandler.state == SimpleGeocoderResultHandler.State.PREVIEWING) {
          this._resultHandler.clear();
        }
      }
    });


    // KeyUp and KeyDown selection for the ResultList
    document.addEventListener("keydown", e => {
      const resultList = this._resultList;
      if (this._resultList == null) { return; }
      const searchQueryField = this._elements.container.querySelector(".ofg-sgc-search-query-field");

      if (document.activeElement != searchQueryField) { return; }
      if (e.key != "ArrowUp" && e.key != "ArrowDown" && e.key != "Enter") { return; }

      const selectedResult = resultList.selected;
      if (selectedResult != null && e.key === "Enter") {
        resultList.go();
        return;
      }

      if(e.key == "ArrowUp") {
        resultList.selectUp();
      } else if(e.key == "ArrowDown") {
        resultList.selectDown();
      }
    });


    this._elements = {container: container};
    this._state = SimpleGeocoderControl.State.COLLAPSED;
    this._provider = provider;
  }

  get state() {
    return this._state;
  }

  set state(state) {
    if (state == this.state) { return; }
    if (state == SimpleGeocoderControl.State.COLLAPSED) {
      this._collapse();
    } else {
      this._expand();
    }
  }

  _expand() {
    const container = this._elements.container;
    const queryField = this._elements.container.querySelector(".ofg-sgc-search-query-field");
    const form = this._elements.container.querySelector(".ofg-sgc-search-form");

    removeClass(queryField, "ofg-sgc-hidden");
    //removeClass(form, "ofg-sgc-hidden");
    queryField.focus();

    replaceClass(container, "ofg-sgc-collapsed", "ofg-sgc-expanded");
    this._state = SimpleGeocoderControl.State.EXPANDED;
  }

  _collapse() {
    const container = this._elements.container;
    const queryField = container.querySelector(".ofg-sgc-search-query-field");
    const form = container.querySelector(".ofg-sgc-search-form");


    this._invalidatePendingSuggestionRequests();
    this.results = [];
    addClass(queryField, "ofg-sgc-hidden");
    replaceClass(container, "ofg-sgc-expanded", "ofg-sgc-collapsed");
    //addClass(form, "ofg-sgc-hidden");
    queryField.value = "";
    queryField.blur();

    if(this._resultHandler.state == SimpleGeocoderResultHandler.State.PREVIEWING) {
      this._resultHandler.state = SimpleGeocoderResultHandler.State.CLEAR;
    }
    this._state = SimpleGeocoderControl.State.COLLAPSED;
  }

  toggleState() {
    if (this.state == SimpleGeocoderControl.State.COLLAPSED) {
      this.state = SimpleGeocoderControl.State.EXPANDED;
    } else {
      this.state = SimpleGeocoderControl.State.COLLAPSED;
    }
  }

  get results() {
    return this._results;
  }

  set results(results) {
    if (this.state === SimpleGeocoderControl.State.COLLAPSED) { throw Error("Illegal state: tried to set search results while the control is collapsed."); }

    this._removeResults();
    if (results.length > 0) {
      this._renderResults(results);
    }
    this._results = results;
  }

  _removeResults() {
    if (this._resultList != null) { this._resultList.remove(); }
    this._resultList = null;
  }

  _renderResults(results) {
    if(!this._resultHandlerIsInitialized) { // TODO can we do this in a better way?
      this.getMap().addLayer(this._resultHandler.layer);
      this._resultHandler.view = this.getMap().getView();
      this._resultHandlerIsInitialized = true;
    }


    if(this._resultList != null) { this._resultList.remove(); }
    this._resultList = document.createElement("result-list");
    this._resultList.render(results);

    this._resultList.addEventListener("selectResult", e => {
      (async () => {
        this._resultHandler.preview(await e.data.resolveGeometry());
      })();
    });

    this._resultList.addEventListener("goResult", e => {
      (async () => {
        this._removeResults();
        this.state = SimpleGeocoderControl.State.COLLAPSED;
        this._resultHandler.go((await e.data.resolveGeometry()));
      })();
    });

    this._elements.container.append(this._resultList);
  }


  async _handleSubmit(e) {
    e.preventDefault();
    const searchField = this._elements.container.querySelector(".ofg-sgc-search-query-field");
    const query = searchField.value;
    const results = await this._provider.search(query);
    if (this.state === SimpleGeocoderControl.STATE.COLLAPSED) { return; }
    this.results = results;
    searchField.blur();
  }
}
SimpleGeocoderControl.State = Object.freeze({"EXPANDED":0, "COLLAPSED":1});

// TODO move to own package
export class ResultCountLimitingProvider {
  constructor(provider, amount) {
    this._provider = provider;
    this._amount = amount;
  }

  async search(query) {
    const results = await this._provider.search(query);
    return results.slice(0, this._amount);
  }

  async suggest(query) {
    const results = await this._provider.autcomplete(query);
    return results.slice(0, this._amount);
  }
}

