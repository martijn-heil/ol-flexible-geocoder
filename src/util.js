export function htmlToElement(html) {
  const template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

export function addClass(element, addend) {
  const values = element.className.split(" ");
  values.push(addend);
  element.className = values.join(" ");
}

export function removeClass(element, subtrahend) {
  element.className = element.className.split(" ").filter(it => it != subtrahend).join(" ");
}

export function replaceClass(element, subtrahend, addend) {
  const values = element.className.split(" ");
  const filtered = values.filter(it => it != subtrahend);
  if(values.length > filtered.length) {
    filtered.push(addend);
    element.className = filtered.join(" ");
  }
}
