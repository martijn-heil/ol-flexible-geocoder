import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Control from 'ol/control/Control';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import {Icon, Style, Stroke} from 'ol/style';
import WKT from 'ol/format/WKT';

import escape from 'escape-html';

import {htmlToElement} from './util';
import {addClass, removeClass, replaceClass} from './util';

import {parse as parseWkt} from 'wkt';


const PDOKLocatieServerURL = "https://geodata.nationaalgeoregister.nl/locatieserver/v3";
export default class PDOKLocatieServerProvider {
  constructor(filterQuery) {
    this._baseUrl = PDOKLocatieServerURL;
    this._filterQuery = filterQuery;
  }

  _getXYfromResponseObj(it) {
    const centroid_epsg_28992_wkt = it.centroide_rd;
    const centroid_epsg_28992_geojson = parseWkt(centroid_epsg_28992_wkt);
    if (centroid_epsg_28992_geojson.type != "Point") { throw Error(); }
    const x = centroid_epsg_28992_geojson.coordinates[0];
    const y = centroid_epsg_28992_geojson.coordinates[1];
    return {x: x, y: y};
  }

  _makeResolveGeometryFunc(id) {
    return (async () => {
      const url = new URL("lookup", this._baseUrl);
      url.searchParams.set("id", id);
      url.searchParams.set("fl", "id,geometrie_rd,centroide_rd");

      const response = await fetch(url);
      if(response.status != 200) {
        throw Error(`Unexpected response status code ${response.status} from ${url}`);
      }
      const json = await response.json();
      const geomWkt = json.response.docs[0].geometrie_rd;
      const formatWkt = new WKT();
      const geomOl = formatWkt.readGeometry(geomWkt, {dataProjection: 'EPSG:28992'}); // EPSG:28992 is Amersfoort / RD New, the Dutch national coordinate system.
      return geomOl;
    });
  }

  async search(query) {
    const url = new URL("free", this._baseUrl);

    url.searchParams.set("q", query);
    url.searchParams.set("indent", "false"); // Don't respond with indented JSON, that only wastes CPU, memory and network resources in the form of whitespace.
    if (this._filterQuery != null && this._filterQuery != "") { url.searchParams.set("fq", this._filterQuery); }


    const response = await fetch(url);
    if (response.status != 200) {
      throw Error(`Unexpected response status code ${response.status} from ${url}`);
    }
    const json = await response.json();
    return json.response.docs.map(it => {
      const centroid = this._getXYfromResponseObj(it);

      return {
        html: escape(it.weergavenaam),
        resolveGeometry: this._makeResolveGeometryFunc(it.id),
        x: centroid.x,
        y: centroid.y,
        metadata: {
          pdok: it,
        }
      };
    });
  }

  async suggest(query) {
    if (query == "") { return []; }
    query = query;
    const url = new URL("suggest", this._baseUrl);

    url.searchParams.set("q", query);
    url.searchParams.set("fl","id,weergavenaam,type,score,centroide_rd"); // Normaal gesproken wordt centroide_rd niet meegezonden, nu wel.
    if (this._filterQuery != null && this._filterQuery != "") { url.searchParams.set("fq", this._filterQuery); }


    const response = await fetch(url);
    if (response.status != 200) {
      throw Error(`Unexpected response status code ${response.status} from ${url}`);
    }
    const json = await response.json();
    return json.response.docs.map(it => {
      const centroid = this._getXYfromResponseObj(it);
      return {
        html: escape(it.weergavenaam),
        resolveGeometry: this._makeResolveGeometryFunc(it.id),
        x: centroid.x,
        y: centroid.y,
        metdata: {
          pdok: it,
        }
      };
    });
  }
}
