export class FilterProvider {
  constructor(provider, filterFunc) {
    this._provider = provider;
    this._filterFunc = filterFunc;
  }

  async suggest(query) {
    const results = await this._provider.suggest(query);
    return this._filterResults(results);
  }

  async search(query) {
    const results = await this._provider.search(query);
    return this._filterResults(results);
  }

  _filterResults(results) {
    return results.filter(it => this._filterFunc(it));
  }
}

export class SimpleBboxFilterProvider extends FilterProvider {
  /*
   * bbox should be of type Array [minx, miny, maxx, maxy]
   */
  constructor(provider, bbox) {
    const _bboxFilterFunc = it => {
      return (it.x > bbox[0] &&
              it.y > bbox[1] &&
              it.x < bbox[2] &&
              it.y < bbox[3]);
    };

    super(provider, _bboxFilterFunc);
    this._bbox = bbox;
  }
}

// TODO properly implement
export class BogusProvider {
  search(query) {
    return Promise.resolve([
      {x: 0, y: 0, proj: 'EPSG:28992', html: escape("Molenstraat 3, Groenstad")},
      {x: 0, y: 0, proj: 'EPSG:28992', html: escape("Kraaijenhoff 22, Driehoeksstad")},
      {x: 0, y: 0, proj: 'EPSG:28992', html: escape("Spieksedijk 3, Alten")},
    ]);
  }

  suggest(query) {
    if (query != "" && "molenstraat".startsWith(query.toLowerCase())) {
      return [{
        x: 0,
        y: 0,
        proj: 'EPSG:28992',
        html: escape("Molenstraat 3, Groenstad"),
      }];
    } else {
      return [];
    }
  }

  hassuggest() {
    return false;
  }
}
